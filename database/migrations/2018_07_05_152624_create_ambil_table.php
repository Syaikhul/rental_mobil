<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambil', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_mobil');
            $table->string('bensin_awal');
            $table->string('total_sewa');
            $table->string('nama_penyewa');
            $table->string('jaminan');
            $table->string('lain_lain');
            $table->dateTime('tanggal_ambil');
            $table->dateTime('tanggal_kembali');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambil');
    }
}
