<?php

// show
Route::get('/', function (){
  return view('index');
});

Route::get('/admin', 'DashboardController@index');
Route::get('/admin/mobil', 'MobilController@index');

// insert
// Route::post('/admin/mobil', 'MobilController@store');

//Update
// Route::get('/admin/mobil/{id}', 'MobilController@edit');
// Route::put('/admin/mobil', 'MobilController@update');

// delete
// Route::delete('/admin/mobil/{id}', 'MobilController@destroy');
