<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>

    <title>@yield('title')</title>
  </head>

  <body>
    <header>
      <nav class="navbar navbar-default">
    		<div class="container-fluid">
    			<div class="navbar-header">
    				<a class="navbar-brand" href="">Rental Mobil</a>
    			</div>
    			<ul class="nav navbar-nav">
    			</ul>
    			<ul class="nav navbar-nav navbar-right">
          <li ><a href="/logout" class="navbar-brand">Login</a></li>
    			</ul>
    		</div>
    	</nav>
    </header>
    <br>


    {{-- bagian conten --}}
    <div class="container-fluid">
        @yield('content')
    </div>

    <br>

    <footer class="page-footer">
      <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="https://facebook.com/msa.syaikul/"> Syaikhul Amin</a>
      </div>
    </footer>

  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
