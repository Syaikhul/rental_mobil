@extends('layout.admin')

@section('title', 'Rental Mobil')

@section('content')

<div class="panel panel-default">
  <div class="panel-heading">
    <h2> Data Mobil
      <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#add">
        Add
      </button>
    </h2>
  </div>

  <div class="panel-body">

    @if(count($errors) > 0 )
        @foreach($errors->all() as $error)
          <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Warning!</strong> {{$error}}
          </div>
        @endforeach
    @else
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Berhasil</strong>
      </div>
    @endif

    <table id="example" class="table table-striped table-bordered" style="width:100%">
      <thead>
        <tr>
          <th width="5%">No</th>
          <th>Image</th>
          <th>Mobil</th>
          <th>Kategori</th>
          <th>Opsi</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>dsadasd</td>
          <td>Toyota</td>
          <td>MPV</td>
          <td>
            <a href="#" class="btn btn-warning">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>

        {{-- awal modal  --}}
        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Mobil</h5>
              </div>
              <form method="post" action="/admin/artikel" enctype="multipart/form-data">
              <div class="modal-body">
                  <div class="form-group">
                    <label>Mobil</label>
                    <input type="text" name="mobil" class="form-control" required="" max="36">
                  </div>
                  <div class="form-group">
                    <label>Kategori</label>
                    <textarea class="form-control" name="kategori" rows="3" required=""></textarea>
                  </div>
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="post">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
            </div>
          </div>
        </div>
        {{-- akhir modal  --}}

      </tbody>
    </table>
  </div>
</div>
@endsection
